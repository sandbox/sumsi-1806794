<?php

/**
 * @file
 * Definition of Drupal\user\Account.
 */

namespace Drupal\user;

use Drupal\Core\Entity\Entity;

/**
 * Defines the account entity class;
 */
class Account extends Entity {

  /**
   * The user ID for this account.
   *
   * @var integer
   */
  public $uid;

  /**
   * The unique username for this account.
   *
   * @var string
   */
  public $username;

  /**
   * The account password (hashed).
   *
   * @var string
   */
  public $pass;

  /**
   * The timestamp when the account was created.
   *
   * @var integer
   */
  public $created;

  /**
   * The timestamp when the account last logged in.
   *
   * @var integer
   */
  public $login;

  /**
   * Whether the user is active (1) or blocked (0).
   *
   * @var integer
   */
  public $status = 1;

  /**
   * The email address used for initial account creation.
   *
   * @var string
   */
  public $init = '';

  /**
   * Implements Drupal\Core\Entity\EntityInterface::id().
   */
  public function id() {
    return $this->uid;
  }
}